#ifndef XLNX_CDMA_H
#define XLNX_CDMA_H

#include <linux/device.h>
#include <linux/dmapool.h>
#include <linux/dmaengine.h>

/* Specific hardware configuration constants */
#define XLNX_CDMA_RST_RST	0x0000000a
#define XLNX_CDMA_DMACR_SINC	0x80000000
#define XLNX_CDMA_DMACR_DINC	0x40000000
#define XLNX_CDMA_DMASR_BSY	0x80000000
#define XLNX_CDMA_DMASR_DBE	0x40000000
#define XLNX_CDMA_ISR_DE	0x00000002
#define XLNX_CDMA_ISR_DD	0x00000001
#define XLNX_CDMA_IER_DEIE	0x00000002
#define XLNX_CDMA_IER_DDIE	0x00000001

struct xlnx_cdma_ld_hw {
	u32 src_addr;
	u32 dst_addr;
	u32 count;
};

struct xlnx_cdma_desc_sw {
	struct xlnx_cdma_ld_hw hw;
	struct list_head node;
	struct list_head tx_list;
	struct dma_async_tx_descriptor async_tx;
};

struct xlnx_cdma_regs {
	u32 rst; 	/* 0x00 Software Reset Register */
	u32 dmacr; 	/* 0x04 DMA Control Register */
	u32 sa;		/* 0x08 Source Address */
	u32 da;		/* 0x0c  Destination Address */
	u32 length;	/* 0x10 Length */
	u32 dmasr;	/* 0x14 DMA Status Register */
	u8 pad[20];
	u32 isr;	/* 0x2C Interrupt Status Register */
	u32 ier;	/* 0x30 Interrupt enable Register */
};

struct xlnx_cdma_chan; 

struct xlnx_cdma_device {
	void __iomem *regs;	/* Register base */
	struct device *dev;
	struct dma_device common;
	struct xlnx_cdma_chan *chan;
	int irq; 		/* Channel IRQ */
};

struct xlnx_cdma_chan {
	struct xlnx_cdma_regs __iomem *regs;
	dma_cookie_t completed_cookie;	/* The maximum cookie completed */
	dma_cookie_t cookie;		/*  The current cookie */
	spinlock_t desc_lock;		/* Descriptor operation lock */
	struct list_head ld_pending; 	/* Link descriptors queue */
	struct list_head ld_running; 	/* Link descriptors queue */
	struct dma_chan common;		/* DMA common channel */
	struct dma_pool *desc_pool;	/* Description pool*/
	struct device *dev;		/* Channel device */
	int irq;			/* Channel IRQ */
	int id;				/* Raw id of this channel */
	struct tasklet_struct tasklet;
	u32 feature;
};

#endif /* XLNX_CDMA_H */
