/*
 * Xilinx XPS Central DMA Controller DMA Engine support
 *
 * Copyright (C) 2011 Alain Péteut, Physikalisches Institut Uni Bern
 *
 * This file is licensed under  the terms of the GNU General Public 
 * License version 2. This program is licensed "as is" without any 
 * warranty of any kind, whether express or implied.
 */

#include <asm/byteorder.h>
#include <asm/delay.h>

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/dma-mapping.h>
#include <linux/dmapool.h>
#include <linux/of_platform.h>
#include <linux/of_address.h>

#include "xlnx_cdma.h"

/* IO accessors */
#define DMA_OUT(addr, val) (iowrite32(val, addr))
#define DMA_IN(addr) (ioread32(addr))

#define to_xlnx_chan(chan) container_of(chan, struct xlnx_cdma_chan, common)
#define to_xlnx_desc(lh) container_of(lh, struct xlnx_cdma_desc_sw, node)
#define tx_to_xlnx_desc(tx) container_of(tx, struct xlnx_cdma_desc_sw, async_tx)

static struct of_platform_driver xlnx_cdma_of_driver;

static void dma_init(struct xlnx_cdma_chan *chan)
{
	u32 tmp;
	/* Reset the channel */
	DMA_OUT(&chan->regs->rst, XLNX_CDMA_RST_RST);
	/* source and destination increment */
	dev_dbg(chan->dev, "%s: DMACR prior set: %#08x\n", __func__,
		DMA_IN(&chan->regs->dmacr));
	DMA_OUT(&chan->regs->dmacr,
		XLNX_CDMA_DMACR_SINC | XLNX_CDMA_DMACR_DINC);
	dev_dbg(chan->dev, "%s: DMACR: %#08x\n", __func__,
		DMA_IN(&chan->regs->dmacr));
	tmp = DMA_IN(&chan->regs->dmacr);
	if (tmp != (XLNX_CDMA_DMACR_SINC | XLNX_CDMA_DMACR_DINC))
		dev_dbg(chan->dev, "%s: dmacr not as written:"
			"expected %#08x, got %#08x\n", __func__,
			XLNX_CDMA_DMACR_SINC | XLNX_CDMA_DMACR_DINC, tmp);
	tmp = DMA_IN(&chan->regs->ier);
	dev_dbg(chan->dev, "%s: IER prior set: %#08x\n", __func__, tmp);
	DMA_OUT(&chan->regs->ier, tmp |XLNX_CDMA_IER_DEIE | XLNX_CDMA_IER_DDIE);
	dev_dbg(chan->dev, "%s: IER: %#08x\n", __func__,
		DMA_IN(&chan->regs->ier));

	/* check reg offsets */
	dev_dbg(chan->dev, "%s: LENGTH@%p\n", __func__, &chan->regs->length);
	dev_dbg(chan->dev, "%s: DMASR@%p\n", __func__, &chan->regs->dmasr);
	dev_dbg(chan->dev, "%s: ISR@%p\n", __func__, &chan->regs->isr);
	dev_dbg(chan->dev, "%s: IER@%p\n", __func__, &chan->regs->ier);
}

static inline void set_sr(struct xlnx_cdma_chan *chan, u32 val)
{
	DMA_OUT(&chan->regs->dmasr, val);
}

static inline u32 get_sr(struct xlnx_cdma_chan *chan)
{
	return DMA_IN(&chan->regs->dmasr);
}

static inline void set_isr(struct xlnx_cdma_chan *chan, u32 val)
{
	DMA_OUT(&chan->regs->isr, val);
}

static inline u32 get_isr(struct xlnx_cdma_chan *chan)
{
	return DMA_IN(&chan->regs->isr);
}

static inline void set_desc_cnt(struct xlnx_cdma_ld_hw *hw, u32 count)
{
	DMA_OUT(&hw->count, count);
}

static inline void set_desc_src(struct xlnx_cdma_ld_hw *hw, dma_addr_t src)
{
	DMA_OUT(&hw->src_addr, src);
}

static inline void set_desc_dst(struct xlnx_cdma_ld_hw *hw, dma_addr_t dst)
{
	DMA_OUT(&hw->dst_addr, dst);
}

static inline void set_cr(struct xlnx_cdma_chan *chan, u32 val)
{
	DMA_OUT(&chan->regs->dmacr, val);
}

static inline u32 get_cr(struct xlnx_cdma_chan *chan)
{
	return DMA_IN(&chan->regs->dmacr);
}

static inline int dma_is_idle(struct xlnx_cdma_chan *chan)
{
	return (!(get_sr(chan) & XLNX_CDMA_DMASR_BSY));
}

static void set_desc_regs(struct xlnx_cdma_chan *chan,
	struct xlnx_cdma_desc_sw *desc)
{
	/* data is already in 'be' */
	chan->regs->sa = desc->hw.src_addr;
	chan->regs->da = desc->hw.dst_addr;
	wmb();
	/* kick it by writing the count register */
	chan->regs->length = desc->hw.count;
}

static void xlnx_cdma_chan_remove(struct xlnx_cdma_chan *chan)
{
	irq_dispose_mapping(chan->irq);
	list_del(&chan->common.device_node);
	iounmap(chan->regs);
	kfree(chan);
}

static void dma_halt(struct xlnx_cdma_chan *chan)
{
	int i;

	for (i = 0; i < 100; i++) {
		if (dma_is_idle(chan))
			return;
		udelay(10);
	}
	dev_err(chan->dev, "DMA halt timeout\n");
}

static void append_ld_queue(struct xlnx_cdma_chan *chan,
	struct xlnx_cdma_desc_sw *desc)
{
	struct xlnx_cdma_desc_sw *tail = to_xlnx_desc(chan->ld_pending.prev);

	if (list_empty(&chan->ld_pending))
		goto splice;
	else
		dev_dbg(chan->dev, "%s: unhandled\n", __func__);
	/*
	 * Add the hardware descriptor to the chain of hardware descriptors
	 * that already exists in memory.
	 *
	 * This will un-set the EOL bit of the existing transaction, and the
	 * last link in this transaction will become the EOL descriptor.
	 */
	
	/*
	FIXME
	set_desc_next(&tail->hw, desc->async_tx.phys);
	*/

	/*
	 * Add the software descriptor and all children to the list
	 * of pending transactions
	 */
splice:
	list_splice_tail_init(&desc->tx_list, &chan->ld_pending);
}


static dma_cookie_t xlnx_cdma_tx_submit(struct dma_async_tx_descriptor *tx)
{
	struct xlnx_cdma_chan *chan = to_xlnx_chan(tx->chan);
	struct xlnx_cdma_desc_sw *desc = tx_to_xlnx_desc(tx);
	struct xlnx_cdma_desc_sw *child;
	unsigned long flags;
	dma_cookie_t cookie;

	spin_lock_irqsave(&chan->desc_lock, flags);

	/*
	 * assign cookies to all of the software descriptors
	 * that make up this transaction
	 */
	cookie = chan->common.cookie;
	list_for_each_entry(child, &desc->tx_list, node) {
		cookie++;
		if (cookie < 0)
			cookie = 1;

		child->async_tx.cookie = cookie;
	}

	chan->common.cookie = cookie;

	/* put this transaction onto the tail of the pending queue */
	append_ld_queue(chan, desc);

	spin_unlock_irqrestore(&chan->desc_lock, flags);

	return cookie;
}

/**
 * xlnx_cdma_alloc_descriptor - Allocate descriptor from channel's DMA pool.
 * @chan : Xilinx CDMA channel
 *
 * Return - The descriptor allocated. NULL for failed.
 */
static struct xlnx_cdma_desc_sw *xlnx_cdma_alloc_descriptor(
	struct xlnx_cdma_chan *chan)
{
	struct xlnx_cdma_desc_sw *desc;
	dma_addr_t pdesc;

	desc = dma_pool_alloc(chan->desc_pool, GFP_ATOMIC, &pdesc);
	if (!desc) {
		dev_dbg(chan->dev, "out of memory for link desc\n");
		return NULL;
	}

	memset(desc, 0, sizeof(*desc));
	INIT_LIST_HEAD(&desc->tx_list);
	dma_async_tx_descriptor_init(&desc->async_tx, &chan->common);
	desc->async_tx.tx_submit = xlnx_cdma_tx_submit;
	desc->async_tx.phys = pdesc;

	return desc;
}

/**
 * xlnx_cdma_alloc_chan_resources - Allocate resources for DMA channel.
 * @chan : Xilinx CDMA channel
 *
 * This function will create a dma pool for descriptor allocation.
 *
 * Return - The number of descriptors allocated.
 */
static int xlnx_cdma_alloc_chan_resources(struct dma_chan *dchan)
{
	struct xlnx_cdma_chan *chan = to_xlnx_chan(dchan);

	/* Has this channel already been allocated? */
	if (chan->desc_pool)
		return 1;

	chan->desc_pool = dma_pool_create("xlnx_cdma_engine_desc_pool",
		chan->dev,
		sizeof(struct xlnx_cdma_desc_sw),
		__alignof__(struct xlnx_cdma_desc_sw), 0);
	if (!chan->desc_pool) {
		dev_err(chan->dev, "unable to allocate channel %d "
				   "descriptor pool\n", chan->id);
		return -ENOMEM;
	}

	/* there is at least one descriptor free to be allocated */
	return 1;
}


/**
 * xlnx_cdma_free_desc_list - Free all descriptors in a queue
 * @chan: Xilinx CDMA channel
 * @list: the list to free
 *
 * LOCKING: must hold chan->desc_lock
 */
static void xlnx_cdma_free_desc_list(struct xlnx_cdma_chan *chan,
				  struct list_head *list)
{
	struct xlnx_cdma_desc_sw *desc, *_desc;

	list_for_each_entry_safe(desc, _desc, list, node) {
		list_del(&desc->node);
		dma_pool_free(chan->desc_pool, desc, desc->async_tx.phys);
	}
}

static void xlnx_cdma_free_desc_list_reverse(struct xlnx_cdma_chan *chan,
					  struct list_head *list)
{
	struct xlnx_cdma_desc_sw *desc, *_desc;

	list_for_each_entry_safe_reverse(desc, _desc, list, node) {
		list_del(&desc->node);
		dma_pool_free(chan->desc_pool, desc, desc->async_tx.phys);
	}
}


/**
 * xlnx_cdma_free_chan_resources - Free all resources of the channel.
 * @chan : Xilinx CDMA channel
 */
static void xlnx_cdma_free_chan_resources(struct dma_chan *dchan)
{
	struct xlnx_cdma_chan *chan = to_xlnx_chan(dchan);
	unsigned long flags;

	dev_dbg(chan->dev, "Free all channel resources.\n");
	spin_lock_irqsave(&chan->desc_lock, flags);
	xlnx_cdma_free_desc_list(chan, &chan->ld_pending);
	xlnx_cdma_free_desc_list(chan, &chan->ld_running);
	spin_unlock_irqrestore(&chan->desc_lock, flags);

	dma_pool_destroy(chan->desc_pool);
	chan->desc_pool = NULL;
}

static struct dma_async_tx_descriptor *
xlnx_cdma_prep_interrupt(struct dma_chan *dchan, unsigned long flags)
{
	struct xlnx_cdma_chan *chan;
	struct xlnx_cdma_desc_sw *new;

	if (!dchan)
		return NULL;

	chan = to_xlnx_chan(dchan);

	new = xlnx_cdma_alloc_descriptor(chan);
	if (!new) {
		dev_err(chan->dev, "No free memory for link descriptor\n");
		return NULL;
	}

	new->async_tx.cookie = -EBUSY;
	new->async_tx.flags = flags;

	/* Insert the link descriptor to the LD ring */
	list_add_tail(&new->node, &new->tx_list);

	/* Set End-of-link to the last link descriptor of new list*/
	/*
	set_ld_eol(new);
	*/

	return &new->async_tx;
}

static struct dma_async_tx_descriptor *
xlnx_cdma_prep_memcpy(struct dma_chan *dchan, dma_addr_t dma_dst,
	dma_addr_t dma_src, size_t len, unsigned long flags)
{
	struct xlnx_cdma_chan *chan;
	struct xlnx_cdma_desc_sw *new;

	if (!dchan)
		return NULL;
	if (!len)
		return NULL;

	/* There is only one channel */
	chan = to_xlnx_chan(dchan);

	/* Allocate the link descriptor from DMA pool */
	new = xlnx_cdma_alloc_descriptor(chan);
	if (!new) {
		dev_err(chan->dev,
			"No free memory for link descriptor\n");
		goto failed;
	}

	if (!len) {
		dev_err(chan->dev,
			"Got zero transfer for length");
		goto failed;
	}

	set_desc_cnt(&new->hw, len);
	set_desc_src(&new->hw, dma_src);
	set_desc_dst(&new->hw, dma_dst);

	new->async_tx.cookie = 0;
	async_tx_ack(&new->async_tx);

	new->async_tx.flags = flags;
	new->async_tx.cookie = -EBUSY;

	return &new->async_tx;

failed:
	if (!new)
		return NULL;

	xlnx_cdma_free_desc_list_reverse(chan, &new->tx_list);
	return NULL;
}

static int xlnx_cdma_device_control(struct dma_chan *dchan,
	enum dma_ctrl_cmd cmd, unsigned long arg)
{
	struct xlnx_cdma_chan *chan;
	unsigned long flags;

	if (cmd != DMA_TERMINATE_ALL)
		return -ENXIO;

	if (!dchan)
		return -EINVAL;

	chan = to_xlnx_chan(dchan);

	dma_halt(chan);

	spin_lock_irqsave(&chan->desc_lock, flags);

	xlnx_cdma_free_desc_list(chan, &chan->ld_pending);
	xlnx_cdma_free_desc_list(chan, &chan->ld_running);

	spin_unlock_irqrestore(&chan->desc_lock, flags);

	return 0;
}


/**
 * xlnx_cdma_update_completed_cookie - Update the completed cookie.
 * @chan : Xilinx CDMA channel
 *
 * CONTEXT: hardirq
 */
static void xlnx_cdma_update_completed_cookie(struct xlnx_cdma_chan *chan)
{
	struct xlnx_cdma_desc_sw *desc;
	unsigned long flags;
	dma_cookie_t cookie;

	spin_lock_irqsave(&chan->desc_lock, flags);

	if (list_empty(&chan->ld_running)) {
		dev_dbg(chan->dev, "no running descriptors\n");
		goto out_unlock;
	}

	/* Get the last descriptor, update the cookie to that */
	desc = to_xlnx_desc(chan->ld_running.prev);
	if (dma_is_idle(chan))
		cookie = desc->async_tx.cookie;
	else {
		cookie = desc->async_tx.cookie - 1;
		if (unlikely(cookie < DMA_MIN_COOKIE))
			cookie = DMA_MAX_COOKIE;
	}

	chan->completed_cookie = cookie;

out_unlock:
	spin_unlock_irqrestore(&chan->desc_lock, flags);
}


/**
 * xlnx_cdma_desc_status - Check the status of a descriptor
 * @chan: Xilinx CDMA channel
 * @desc: DMA SW descriptor
 *
 * This function will return the status of the given descriptor
 */
static enum dma_status xlnx_cdma_desc_status(struct xlnx_cdma_chan *chan,
					  struct xlnx_cdma_desc_sw *desc)
{
	return dma_async_is_complete(desc->async_tx.cookie,
				     chan->completed_cookie,
				     chan->common.cookie);
}


/**
 * xlnx_cdma_chan_ld_cleanup - Clean up link descriptors
 * @chan : Xilinx CDMA channel
 *
 * This function clean up the ld_queue of DMA channel.
 */
static void xlnx_cdma_chan_ld_cleanup(struct xlnx_cdma_chan *chan)
{
	struct xlnx_cdma_desc_sw *desc, *_desc;
	unsigned long flags;

	spin_lock_irqsave(&chan->desc_lock, flags);

	dev_dbg(chan->dev, "chan completed_cookie = %d\n",
		chan->completed_cookie);
	list_for_each_entry_safe(desc, _desc, &chan->ld_running, node) {
		dma_async_tx_callback callback;
		void *callback_param;

		if (xlnx_cdma_desc_status(chan, desc) == DMA_IN_PROGRESS)
			break;

		/* Remove from the list of running transactions */
		list_del(&desc->node);

		/* Run the link descriptor callback function */
		callback = desc->async_tx.callback;
		callback_param = desc->async_tx.callback_param;
		if (callback) {
			/* FIXME */
			spin_unlock_irqrestore(&chan->desc_lock, flags);
			dev_dbg(chan->dev, "LD %p callback\n", desc);
			callback(callback_param);
			spin_lock_irqsave(&chan->desc_lock, flags);
		}

		/* Run any dependencies, then free the descriptor */
		dma_run_dependencies(&desc->async_tx);
		dma_pool_free(chan->desc_pool, desc, desc->async_tx.phys);
	}

	spin_unlock_irqrestore(&chan->desc_lock, flags);
}


/**
 * xlnx_cdma_chan_xfer_ld_queue - transfer any pending transactions
 * @chan : Xilinx CDMA channel
 *
 * This will make sure that any pending transactions will be run.
 * If the DMA controller is idle, it will be started. Otherwise,
 * the DMA controller's interrupt handler will start any pending
 * transactions when it becomes idle.
 */
static void xlnx_cdma_chan_xfer_ld_queue(struct xlnx_cdma_chan *chan)
{
	struct xlnx_cdma_desc_sw *desc;
	unsigned long flags;

	dev_dbg(chan->dev, "%s\n", __func__);
	
	spin_lock_irqsave(&chan->desc_lock, flags);

	/*
	 * If the list of pending descriptors is empty, then we
	 * don't need to do any work at all
	 */
	if (list_empty(&chan->ld_pending)) {
		dev_dbg(chan->dev, "no pending LDs\n");
		goto out_unlock;
	}

	/*
	 * The DMA controller is not idle, which means the interrupt
	 * handler will start any queued transactions when it runs
	 * at the end of the current transaction
	 */
	if (!dma_is_idle(chan)) {
		dev_dbg(chan->dev, "DMA controller still busy\n");
		goto out_unlock;
	}

	/*
	 * If there are some link descriptors which have not been
	 * transferred, we need to start the controller
	 */

	/*
	 * Move all elements from the queue of pending transactions
	 * onto the list of running transactions
	 */
	desc = list_first_entry(&chan->ld_pending,
		struct xlnx_cdma_desc_sw, node);
	list_splice_tail_init(&chan->ld_pending, &chan->ld_running);

	/*  start the DMA controller */
	set_desc_regs(chan, desc);

out_unlock:
	spin_unlock_irqrestore(&chan->desc_lock, flags);
}


/**
 * xlnx_cdma_memcpy_issue_pending - Issue the DMA start command
 * @chan : Xilinx CDMA channel
 */
static void
xlnx_cdma_memcpy_issue_pending(struct dma_chan *dchan)
{
	struct xlnx_cdma_chan *chan = to_xlnx_chan(dchan);
	xlnx_cdma_chan_xfer_ld_queue(chan);
}


/**
 * xlnx_cdma_tx_status - Determine the DMA status
 * @chan : Xilinx CDMA channel
 */
static enum dma_status
xlnx_cdma_tx_status(struct dma_chan *dchan, 
	dma_cookie_t cookie,
	struct dma_tx_state *txstate)
{
	struct xlnx_cdma_chan *chan = to_xlnx_chan(dchan);
	dma_cookie_t last_used;
	dma_cookie_t last_complete;
	
	pr_info("%s: running\n", __func__);
	xlnx_cdma_chan_ld_cleanup(chan);

	last_used = dchan->cookie;
	last_complete = chan->completed_cookie;

	dma_set_tx_state(txstate, last_complete, last_used, 0);

	return dma_async_is_complete(cookie, last_complete, last_used);
}

static void
dma_do_tasklet(unsigned long data)
{
	struct xlnx_cdma_chan *chan = (struct xlnx_cdma_chan *)data;
	/* FIXME spinlcock bad magic observed! */
	xlnx_cdma_chan_ld_cleanup(chan);
}

static irqreturn_t
xlnx_cdma_intr_handler(int irq, void *data)
{
	struct xlnx_cdma_device *fdev = data;
	struct xlnx_cdma_chan *chan;
	int update_cookie = 0;
	int xfer_ld_q = 0;
	u32 stat;

	/* there is only one channel */
	chan = fdev->chan;

	/* save and clean the interrupt status register */
	stat = get_isr(chan);
	/* ack (toggle on write) */
	set_isr(chan, stat);

	dev_dbg(chan->dev, "irq: channel #%d, state = %#08x\n", chan->id, stat);

	stat &= (XLNX_CDMA_ISR_DE | XLNX_CDMA_ISR_DD);
	if (!stat) {
		dev_dbg(chan->dev, "irq: channel #%d, no ISR flag set\n",
			chan->id);
		return IRQ_NONE;
	}

	if (stat & XLNX_CDMA_ISR_DE) {
		dev_dbg(chan->dev, "irq: DMA Error\n");
		/* Redo */
		/* update_cookie = 1; */
		xfer_ld_q = 1;
		stat &= ~XLNX_CDMA_ISR_DE;
	}

	if (stat & XLNX_CDMA_ISR_DD) {
		update_cookie = 1;
		stat &= ~XLNX_CDMA_ISR_DD;
	}

	if (update_cookie)
		xlnx_cdma_update_completed_cookie(chan);
	if (xfer_ld_q)
		xlnx_cdma_chan_xfer_ld_queue(chan);
	if (stat)
		dev_dbg(chan->dev, "irq: unhandled ISR %#08x\n", stat);

	dev_dbg(chan->dev, "irq: Exit\n");
	tasklet_schedule(&chan->tasklet);
	return IRQ_HANDLED;
}

static void xlnx_cdma_free_irq(struct xlnx_cdma_device *fdev)
{
	if (fdev->irq != NO_IRQ) {
		dev_dbg(fdev->dev, "free controller IRQ\n");
		free_irq(fdev->irq, fdev);
	}
}

static int xlnx_cdma_request_irqs(struct xlnx_cdma_device *fdev)
{
	int ret;

	if (fdev->irq != NO_IRQ) {
		dev_dbg(fdev->dev, "request controller IRQ\n");
		ret = request_irq(fdev->irq, xlnx_cdma_intr_handler, IRQF_SHARED,
			xlnx_cdma_of_driver.driver.name, fdev);
	} else
		ret = -ENODEV;

	return ret;
}

static int __devinit xlnx_cdma_chan_probe(struct xlnx_cdma_device *fdev)
{
	struct xlnx_cdma_chan *chan;
	struct resource res;
	int err;

	chan = kzalloc(sizeof(*chan), GFP_KERNEL);
	if (!chan) {
		dev_err(fdev->dev, "not enough memory for CDMA channel\n");
		err = -ENOMEM;
		goto out_return;
	}

	chan->regs = of_iomap(fdev->dev->of_node, 0);
	if (!chan->regs) {
		dev_err(fdev->dev, "unable to iomap registers\n");
		err = -ENOMEM;
		goto out_free_chan;
	}

	err = of_address_to_resource(fdev->dev->of_node, 0, &res);
	if (err) {
		dev_err(fdev->dev, "unable to find 'reg' property\n");
		goto out_iounmap_regs;
	}

	chan->dev = fdev->dev;
	chan->id = 0;

	fdev->chan = chan;
	tasklet_init(&chan->tasklet, dma_do_tasklet, (unsigned long)chan);

	dma_init(chan);

	spin_lock_init(&chan->desc_lock);
	INIT_LIST_HEAD(&chan->ld_pending);
	INIT_LIST_HEAD(&chan->ld_running);

	chan->common.device = &fdev->common;

	chan->irq = irq_of_parse_and_map(fdev->dev->of_node, 0);

	list_add_tail(&chan->common.device_node, &fdev->common.channels);
	fdev->common.chancnt++;

	dev_info(fdev->dev, "#%d (%s), irq %d\n", chan->id,
		xlnx_cdma_of_driver.driver.name, chan->irq);
	return 0;

out_iounmap_regs:
	iounmap(chan->regs);
out_free_chan:
	kfree(chan);
out_return:
	return err;
}

static int __devinit xlnx_cdma_of_probe(struct platform_device *op,
					const struct of_device_id *match)
{
	struct xlnx_cdma_device *fdev;
	int err;

	fdev = kzalloc(sizeof(*fdev), GFP_KERNEL);
	if (!fdev) {
		dev_err(&op->dev, "Not enough memory for device\n");
		err = -ENOMEM;
		goto out_return;
	}
	fdev->dev = &op->dev;
	INIT_LIST_HEAD(&fdev->common.channels);

	fdev->regs = of_iomap(op->dev.of_node, 0);
	if (!fdev->regs) {
		dev_err(&op->dev, "unable to iomap registers\n");
		err = -ENOMEM;
		goto out_free_fdev;
	}

	fdev->irq = irq_of_parse_and_map(op->dev.of_node, 0);

	/* CDMA only does memcpy */
	dma_cap_set(DMA_MEMCPY, fdev->common.cap_mask);
	fdev->common.device_alloc_chan_resources = xlnx_cdma_alloc_chan_resources;
	fdev->common.device_free_chan_resources = xlnx_cdma_free_chan_resources;
	fdev->common.device_prep_dma_interrupt = xlnx_cdma_prep_interrupt;
	fdev->common.device_prep_dma_memcpy = xlnx_cdma_prep_memcpy;
	fdev->common.device_tx_status = xlnx_cdma_tx_status;
	fdev->common.device_issue_pending = xlnx_cdma_memcpy_issue_pending;
	fdev->common.device_control = xlnx_cdma_device_control;
	fdev->common.dev = &op->dev;

	dev_set_drvdata(&op->dev, fdev);

	/* there is only one channel */
	xlnx_cdma_chan_probe(fdev);

	err = xlnx_cdma_request_irqs(fdev);
	if (err) {
		dev_err(fdev->dev, "unable to request IRQ\n");
		goto out_free_irq;
	}

	dma_async_device_register(&fdev->common);
	return 0;

out_free_irq:
	irq_dispose_mapping(fdev->irq);
	iounmap(fdev->regs);
out_free_fdev:
	kfree(fdev);
out_return:
	return err;
}

static int xlnx_cdma_of_remove(struct platform_device *op)
{
	struct xlnx_cdma_device *fdev;

	fdev = dev_get_drvdata(&op->dev);
	dma_async_device_unregister(&fdev->common);
	xlnx_cdma_free_irq(fdev);
	xlnx_cdma_chan_remove(fdev->chan);
	iounmap(fdev->regs);
	dev_set_drvdata(&op->dev, NULL);
	kfree(fdev);

	return 0;
}

static const struct of_device_id xlnx_cdma_of_ids[] = {
	{ .compatible = "xlnx,xps-central-dma-2.03.a", },
	{}
};

static struct of_platform_driver xlnx_cdma_of_driver = {
	.driver = {
		.name = "xlnx-central-dma",
		.owner = THIS_MODULE,
		.of_match_table = xlnx_cdma_of_ids,
	},
	.probe = xlnx_cdma_of_probe,
	.remove = xlnx_cdma_of_remove,
};

static __init int xlnx_cdma_init(void)
{
	int ret;

	ret = of_register_platform_driver(&xlnx_cdma_of_driver);
	if (ret)
		pr_err("xlnx_cdma: failed to register platform driver\n");

	return ret;
}

static void __exit xlnx_cdma_exit(void)
{
	of_unregister_platform_driver(&xlnx_cdma_of_driver);
}

subsys_initcall(xlnx_cdma_init);
module_exit(xlnx_cdma_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Physikalisches Institut WP");
MODULE_DESCRIPTION("Xilinx XPS CDMA driver");

