/*
 *
 *
 *
 *
 */
#ifndef XLNX_CDMA_H
#define XLNX_CDMA_H

#include <linux/device.h>
#include <linux/dmapool.h>
#include <linux/dmaengine.h>

/* Specific hardware configuration constants */

#define XLNX_CDMA_RST_RST	0x0000000a
#define XLNX_CDMA_DMACR_SINC	0x80000000
#define XLNX_CDMA_DMACR_DINC	0x40000000
#define XLNX_CDMA_DMASR_BSY	0x80000000
#define XLNX_CDMA_DMASR_DBE	0x40000000
#define XLNX_CDMA_ISR_DE	0x00000002
#define XLNX_CDMA_ISR_DD	0x00000001
#define XLNX_CDMA_IER_DEIE	0x00000002
#define XLNX_CDMA_IER_DDIE	0x00000001

#define DESCS_PER_CHANNEL	16 /* FIXME */

struct xlnx_cdma_regs {
	u32		rst; 	/* 0x00 Software Reset Register */
	u32		dmacr; 	/* 0x04 DMA Control Register */
	u32		sa;	/* 0x08 Source Address */
	u32		da;	/* 0x0c  Destination Address */
	u32		length;	/* 0x10 Length */
	u32		dmasr;	/* 0x14 DMA Status Register */
	u8		pad[20];
	u32		isr;	/* 0x2C Interrupt Status Register */
	u32		ier;	/* 0x30 Interrupt enable Register */
};

/**
 * struct xlnx_cdma_chan - internal cdma representation of a DMA channel
 * @chan: dma_chan structure
 * @ch_regs: MMIO register space pinter to channel register
 * @lock: channel spinlock
 * @completed: DMA cookie
 * @active_list: current active descriptors
 * @queue: current queued up descriptors
 * @free_list: current free descriptors
 * @descs_allocated: total number of descriptors allocated
 * @dma: dma device structure pointer
 * @busy: bool representing if ch is busy (active txn) or not
 * @in_use: bool representing if ch is in use or not
 * @raw_isr: raw isr interrupt received
 */

struct xlnx_cdma_chan {
	struct dma_chan 	chan;
	struct xlnx_cdma_regs __iomem *ch_regs;
	spinlock_t		lock;
	dma_cookie_t		completed;
	struct list_head	active_list;
	struct list_head	queue;
	struct list_head	free_list;
	unsigned int		descs_allocated;
	struct xlnx_cdma_device	*dma;
	bool			busy;
	bool			in_use;
	u32			raw_isr;
};


/**
 * struct xlnx_cdma_device - internal representation of a DMA device
 * @dev: PLB device
 * @dma_base: MMIO register space pointer of DMA
 * @dma_pool: allocation of DMA descriptors
 * @common: embedded struct dma_device
 * @tasklet: DMA tasklet
 * @ch: channel data
 * @irq: interrupt
 */
struct xlnx_cdma_device {
	struct device		*pdev;
	void __iomem 		*dma_base;
	struct dma_pool		*dma_pool;
	struct dma_device	common;
	struct tasklet_struct	tasklet;
	struct xlnx_cdma_chan	ch;
	int irq;
};

struct xlnx_cdma_desc {
	void __iomem		*block; /* ch ptr */
	struct list_head	desc_node;
	struct dma_async_tx_descriptor txd;
	size_t			len;
	dma_addr_t		sar;
	dma_addr_t		dar;
	dma_addr_t		next;
/*	enum dma_data_direction	dirn; */
	enum dma_status		status;
};

static inline int test_ch_busy(void __iomem *dma)
{
	u32 sr = ioread32(((struct xlnx_cdma_regs *)dma)->dmasr);
	return (sr & XLNX_CDMA_DMASR_BSY); 
}

static inline struct xlnx_cdma_chan *to_xlnx_cdma_chan(
	struct dma_chan *chan)
{
	return container_of(chan, struct xlnx_cdma_chan, chan);
}

static inline struct xlnx_cdma_device *to_xlnx_cdma_device(
	struct dma_device *common)
{
	return container_of(common, struct xlnx_cdma_device, common);
}

static inline struct xlnx_cdma_desc *to_xlnx_cdma_desc(
	struct dma_async_tx_descriptor *txd)
{
	return container_of(txd, struct xlnx_cdma_desc, txd);
}
#endif /* XLNX_CDMA_H */
