/*
 * 
 * derived from intel_mid_dma.c
 * 
 *
 *
 *
 *
 */

#include <asm/byteorder.h>
#include <asm/delay.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/dma-mapping.h>
#include <linux/dmapool.h>
#include <linux/of_platform.h>
#include <linux/of_address.h>

#include "xlnx_cdma_v2.h" 

/* IO accessors */
#define DMA_OUT(addr, val) (iowrite32(val, addr))
#define DMA_IN(addr) (ioread32(addr))


/*****************************************************************************
CDMA interrupt Functions */

/**
 * enable_dma_interrupt
 * @chan: dma channel
 */
static void enable_dma_interrupt(struct xlnx_cdma_chan *chan)
{
	iowrite32(XLNX_CDMA_IER_DEIE | XLNX_CDMA_IER_DDIE,
		&chan->ch_regs->ier);
}

/**
 * disable_dma_interrupt
 * @chan: dma channel
 */
static void disable_dma_interrupt(struct xlnx_cdma_chan *chan)
{
	iowrite32((XLNX_CDMA_IER_DEIE | XLNX_CDMA_IER_DDIE) ^
		(XLNX_CDMA_IER_DEIE | XLNX_CDMA_IER_DDIE), 
		&chan->ch_regs->ier);
}

/*****************************************************************************
DMA channel helper Functions*/
/**
 * chan_desc_get - get a descriptor
 * @chan: dma channel for which descriptor is required
 *
 * Obtain a descriptor for the channel. Returns NULL if none are free.
 * Once the descriptor is returned it is private until put on another
 * list or freed
 */
static struct xlnx_cdma_desc *chan_desc_get(struct xlnx_cdma_chan *chan)
{
	struct xlnx_cdma_desc *desc, *_desc;
	struct xlnx_cdma_desc *ret = NULL;

	spin_lock_bh(&chan->lock);
	list_for_each_entry_safe(desc, _desc, &chan->free_list, desc_node) {
		if (async_tx_test_ack(&desc->txd)) {
			list_del(&desc->desc_node);
			ret = desc;
			break;
		}
	}
	spin_unlock_bh(&chan->lock);
	return ret;
}

/**
 * chan_desc_put - put a descriptor
 * @chan: dma channel for which descriptor is required
 * @desc: descriptor to put
 *
 * Return a descriptor from lwn_desc_get back to the free pool
 */
static void chan_desc_put(struct xlnx_cdma_chan *chan,
			struct xlnx_cdma_desc *desc)
{
	if (desc) {
		spin_lock_bh(&chan->lock);
		list_add_tail(&desc->desc_node, &chan->free_list);
		spin_unlock_bh(&chan->lock);
	}
}


/**
 * chan_dostart	- begin a DMA transaction
 * @chan: channel for which txn is to be started
 * @first: first descriptor of series
 *
 * Load a transaction into the engine. This must be called with midc->lock
 * held and bh disabled.
 */
static void chan_dostart(struct xlnx_cdma_chan *chan,
	struct xlnx_cdma_desc *first)
{
	struct xlnx_cdma_device *cdma = to_xlnx_cdma_device(chan->chan.device);
	u32 status = ioread32(&chan->ch_regs->dmasr);

	/*  channel is idle */
	if (chan->busy) {
		dev_err(cdma->pdev, "%s: channel is busy in start\n", __func__);
		dev_err(cdma->pdev, "%s: IER: %#08x\n", __func__,
			ioread32(&chan->ch_regs->ier));
		/* The tasklet will hopefully advance the queue... */
		return;
	}
	chan->busy = true;
	status = ioread32(&chan->ch_regs->ier);
	dev_dbg(chan->dma->pdev, "%s: IER: %#08x\n", __func__, status);
	/* write registers and en */
	iowrite32(first->sar, &chan->ch_regs->sa);
	iowrite32(first->dar, &chan->ch_regs->da);
	wmb();
	dev_dbg(cdma->pdev, "%s: length: %#08x\n", __func__, first->len);
	iowrite32(first->len, &chan->ch_regs->length);

	first->status = DMA_IN_PROGRESS;
}


/**
 * xlnx_cdma_descriptor_complete - process completed descriptor
 * @chan: channel owning the descriptor
 * @desc: the descriptor itself
 *
 * Process a completed descriptor and perform any callbacks upon
 * the completion. The completion handling drops the lock during the
 * callbacks but must be called with the lock held.
 */
static void xlnx_cdma_descriptor_complete(struct xlnx_cdma_chan *chan,
	       struct xlnx_cdma_desc *desc)
{
	struct dma_async_tx_descriptor *txd = &desc->txd;
	dma_async_tx_callback callback_txd = NULL;
	void *param_txd = NULL;

	chan->completed = txd->cookie;
	callback_txd = txd->callback;
	param_txd = txd->callback_param;

	spin_unlock_bh(&chan->lock);
	if (callback_txd) {
		dev_dbg(chan->dma->pdev, "%s: TXD callback set ... calling\n", 
			__func__);
		callback_txd(param_txd);
	}
	if (chan->raw_isr & XLNX_CDMA_ISR_DD) {
		desc->status = DMA_SUCCESS;
		list_move(&desc->desc_node, &chan->free_list);
		chan->busy = false;
	}
	spin_lock_bh(&chan->lock);

}


/**
 * xlnx_cdma_descriptors - check the descriptors in channel
 * mark completed when tx is completete
 * @chan: channel to scan
 *
 * Walk the descriptor chain for the device and process any entries
 * that are complete.
 */
static void xlnx_cdma_scan_descriptors(struct xlnx_cdma_chan *chan)
{
	struct xlnx_cdma_desc *desc = NULL, *_desc = NULL;

	/* tx is complete */
	list_for_each_entry_safe(desc, _desc, &chan->active_list, desc_node) {
		if (desc->status == DMA_IN_PROGRESS) {
			xlnx_cdma_descriptor_complete(chan, desc);
		}
	}
	return;
}


/*****************************************************************************
DMA engine callback Functions */
/**
 * xlnx_cdma_tx_submit - callback to submit DMA transaction
 * @tx: dma engine descriptor
 *
 * Submit the DMA trasaction for this descriptor, start if ch idle
 */
static dma_cookie_t xlnx_cdma_tx_submit(struct dma_async_tx_descriptor *tx)
{
	struct xlnx_cdma_desc *desc = to_xlnx_cdma_desc(tx);
	struct xlnx_cdma_chan *chan = to_xlnx_cdma_chan(tx->chan);
	dma_cookie_t cookie;

	spin_lock_bh(&chan->lock);
	cookie = chan->chan.cookie;

	if (++cookie < 0)
		cookie = 1;

	chan->chan.cookie = cookie;
	desc->txd.cookie = cookie;


	if (list_empty(&chan->active_list)) 
		list_add_tail(&desc->desc_node, &chan->active_list);
	else 
		list_add_tail(&desc->desc_node, &chan->queue);

	chan_dostart(chan, desc);
	spin_unlock_bh(&chan->lock);

	return cookie;
}

/**
 * xlnx_cdma_issue_pending - callback to issue pending txn
 * @chan: chan where pending trascation needs to be checked and submitted
 *
 * Call for scan to issue pending descriptors
 */
static void xlnx_cdma_issue_pending(struct dma_chan *chan)
{
	struct xlnx_cdma_chan *ch = to_xlnx_cdma_chan(chan);

	spin_lock_bh(&ch->lock);
	if (!list_empty(&ch->queue))
		xlnx_cdma_scan_descriptors(ch);
	spin_unlock_bh(&ch->lock);
}

/**
 * xlnx_cdma_tx_status - Return status of txn
 * @chan: chan for where status needs to be checked
 * @cookie: cookie for txn
 * @txstate: DMA txn state
 *
 * Return status of DMA txn
 */
static enum dma_status xlnx_cdma_tx_status(struct dma_chan *chan,
	dma_cookie_t cookie,
	struct dma_tx_state *txstate)
{
	struct xlnx_cdma_chan *ch = to_xlnx_cdma_chan(chan);
	dma_cookie_t last_used;
	dma_cookie_t last_complete;
	int ret;

	spin_lock_bh(&ch->lock);
	last_complete = ch->completed;
	last_used = chan->cookie;
	spin_unlock_bh(&ch->lock);

	ret = dma_async_is_complete(cookie, last_complete, last_used);
	if (ret != DMA_SUCCESS) {
		spin_lock_bh(&ch->lock);
		xlnx_cdma_scan_descriptors(ch);

		last_complete = ch->completed;
		last_used = chan->cookie;
		spin_unlock_bh(&ch->lock);
		ret = dma_async_is_complete(cookie, last_complete, last_used);
	}

	if (txstate) {
		txstate->last = last_complete;
		txstate->used = last_used;
		txstate->residue = 0;
	}
	return ret;
}


/**
 * xlnx_cdma_device_control - DMA device control
 * @chan: chan for DMA control
 * @cmd: control cmd
 * @arg: cmd arg value
 *
 * Perform DMA control command
 */
static int xlnx_cdma_device_control(struct dma_chan *chan,
	enum dma_ctrl_cmd cmd, unsigned long arg)
{
	struct xlnx_cdma_chan *ch = to_xlnx_cdma_chan(chan);
	struct xlnx_cdma_device *cdma = to_xlnx_cdma_device(chan->device);
	struct xlnx_cdma_desc *desc, *_desc;

	dev_dbg(cdma->pdev, "%s:\n", __func__);
	if (cmd != DMA_TERMINATE_ALL)
		return -ENXIO;

	spin_lock_bh(&ch->lock);
	if (ch->busy == false) {
		spin_unlock_bh(&ch->lock);
		return 0;
	}

	ch->busy = false;
	/* Disable interrupts */
	disable_dma_interrupt(ch);
	ch->descs_allocated = 0;

	spin_unlock_bh(&ch->lock);
	list_for_each_entry_safe(desc, _desc, &ch->active_list, desc_node) {
		list_move(&desc->desc_node, &ch->free_list);
	}
	return 0;
}


/**
 * xlnx_cdma_prep_slave_sg - Prep slave sg txn
 * @chan: chan for DMA transfer
 * @sgl: scatter gather list
 * @sg_len: length of sg txn
 * @direction: DMA transfer dirtn
 * @flags: DMA flags
 *
 * Do DMA sg txn: NOT supported now
 */
static struct dma_async_tx_descriptor *xlnx_cdma_prep_slave_sg(
	struct dma_chan *chan,
	struct scatterlist *sgl,
	unsigned int sg_len,
	enum dma_data_direction direction,
	unsigned long flags)
{
	/* not supported now */
	return NULL;
}


/**
 * xlnx_cdma_prep_memcpy - Prep memcpy txn
 * @chan: chan for DMA transfer
 * @dest: destn address
 * @src: src address
 * @len: DMA transfer len
 * @flags: DMA flags
 *
 * Perform a DMA memcpy. Note we support slave periphral DMA transfers only
 * The periphral txn details should be filled in slave structure properly
 * Returns the descriptor for this txn
 */
static struct dma_async_tx_descriptor *xlnx_cdma_prep_memcpy(
	struct dma_chan *chan, dma_addr_t dest, dma_addr_t src,
	size_t len, unsigned long flags)
{
	struct xlnx_cdma_device *cdma = to_xlnx_cdma_device(chan->device);
	struct xlnx_cdma_chan *ch;
	struct xlnx_cdma_desc *desc = NULL;

	dev_dbg(cdma->pdev, "prep for memcpy\n");
	BUG_ON(!chan);
	if (!len)
		return NULL;

	ch = to_xlnx_cdma_chan(chan);
	BUG_ON(!ch);

	dev_dbg(cdma->pdev, "called for channel #%d length %x\n", 0, len);

	enable_dma_interrupt(ch);

	desc = chan_desc_get(ch);
	if (!desc)
		goto err_desc_get;
	desc->sar = src;
	desc->dar = dest;
	desc->len = len;
	return &desc->txd;

err_desc_get:
	dev_err(cdma->pdev, "failed to get desc\n");
	chan_desc_put(ch, desc);
	return NULL;
}


/**
 * xlnx_cdma_free_chan_resources - Frees dma resources
 * @chan: chan requiring attention
 *
 * Frees the allocated resources on this DMA chan
 */
static void xlnx_cdma_free_chan_resources(struct dma_chan *chan)
{
	struct xlnx_cdma_chan *ch = to_xlnx_cdma_chan(chan);
	struct xlnx_cdma_device *cdma = to_xlnx_cdma_device(chan->device);
	struct xlnx_cdma_desc *desc, *_desc;

	if (ch->busy) {
		/*trying to free ch in use!!!!!*/
		pr_err("ERR_MDMA: trying to free ch in use\n");
	}

	spin_lock_bh(&ch->lock);
	ch->descs_allocated = 0;
	list_for_each_entry_safe(desc, _desc, &ch->active_list, desc_node) {
		list_del(&desc->desc_node);
		dma_pool_free(cdma->dma_pool, desc, desc->txd.phys);
	}
	list_for_each_entry_safe(desc, _desc, &ch->free_list, desc_node) {
		list_del(&desc->desc_node);
		dma_pool_free(cdma->dma_pool, desc, desc->txd.phys);
	}
	list_for_each_entry_safe(desc, _desc, &ch->queue, desc_node) {
		list_del(&desc->desc_node);
		dma_pool_free(cdma->dma_pool, desc, desc->txd.phys);
	}
	spin_unlock_bh(&ch->lock);
	ch->in_use = false;
	ch->busy = false;
	/* Disable CH interrupts */
	disable_dma_interrupt(ch);	
}

/**
 * xlnx_cdma_alloc_chan_resources - Allocate dma resources
 * @chan: chan requiring attention
 *
 * Allocates DMA resources on this chan
 * Return the descriptors allocated
 */
static int xlnx_cdma_alloc_chan_resources(struct dma_chan *chan)
{
	struct xlnx_cdma_chan *ch = to_xlnx_cdma_chan(chan);
	struct xlnx_cdma_device *cdma = to_xlnx_cdma_device(chan->device);
	struct xlnx_cdma_desc *desc;
	dma_addr_t phys;
	int i = 0;

	ch->completed = chan->cookie = 1;

	spin_lock_bh(&ch->lock);
	while (ch->descs_allocated < DESCS_PER_CHANNEL) {
		spin_unlock_bh(&ch->lock);
		desc = dma_pool_alloc(cdma->dma_pool, GFP_KERNEL, &phys);
		if (!desc) {
			dev_err(cdma->pdev, "%s: desc failed\n", __func__);
			return -ENOMEM;
			/*check*/
		}
		dma_async_tx_descriptor_init(&desc->txd, chan);
		desc->txd.tx_submit = xlnx_cdma_tx_submit;
		desc->txd.flags = DMA_CTRL_ACK;
		desc->txd.phys = phys;
		spin_lock_bh(&ch->lock);
		i = ++ch->descs_allocated;
		list_add_tail(&desc->desc_node, &ch->free_list);
	}
	spin_unlock_bh(&ch->lock);
	ch->in_use = true;
	ch->busy = false;
	dev_dbg(cdma->pdev, "%s: desc alloc done ret: %d desc\n",__func__, i);
	return i;
}

/**
 * xlnx_cdma_handle_error - Handle DMA txn error
 * @chan: chan where error occured
 *
 * Scan the descriptor for error
 */
static void xlnx_cdma_handle_error(struct xlnx_cdma_chan *chan)
{
	xlnx_cdma_scan_descriptors(chan);
}


/**
 * dma_tasklet - DMA interrupt tasklet
 * @data: tasklet arg (the controller structure)
 *
 * Scan the controller for interrupts for completion/error
 * Clear the interrupt and call for handling completion/error
 */
static void dma_tasklet(unsigned long data)
{
	struct xlnx_cdma_device *cdma = (struct xlnx_cdma_device *)data;
	struct  xlnx_cdma_chan *ch = &cdma->ch;
	u32 status;

	if (!cdma) {
		pr_err("%s: Null param\n", __func__);
		return;
	}
	ch->raw_isr = status = ioread32(&ch->ch_regs->isr);
	dev_dbg(cdma->pdev, "%s: ISR: %#08x\n",__func__, status);

	if (status & XLNX_CDMA_ISR_DD) {
		/* txn interrupt */
		dev_dbg(cdma->pdev, "Tx complete interrupt #%d\n", 0);
		spin_lock_bh(&ch->lock);
		/* clearing this interrupt */
		iowrite32(XLNX_CDMA_ISR_DD, &ch->ch_regs->isr);
		xlnx_cdma_scan_descriptors(ch);
		dev_dbg(cdma->pdev, "Scan of desc... complete\n");
		spin_unlock_bh(&ch->lock);
	}
	if (status & XLNX_CDMA_ISR_DE) {
		/* err interrupt */
		dev_dbg(cdma->pdev, "DMA error interrupt #%d\n", 0);
		spin_lock_bh(&ch->lock);
		/* clering this interrupt */
		iowrite32(XLNX_CDMA_ISR_DE, &ch->ch_regs->isr);
		xlnx_cdma_handle_error(ch);
		spin_unlock_bh(&ch->lock);
	}
	dev_dbg(cdma->pdev, "%s: Exiting\n", __func__);
	return;
}

/**
 * xlnx_cdma_interrupt - DMA ISR
 * @irq: IRQ where interrupt occurred
 * @data: ISR cllback data (the controller structure)
 *
 * See if this is our interrupt if so then schedule the tasklet
 * otherwise ignore
 */
static irqreturn_t xlnx_cdma_interrupt(int irq, void *data)
{
	struct xlnx_cdma_device *cdma = (struct xlnx_cdma_device *)data;
	struct xlnx_cdma_chan *ch = &cdma->ch;
	u32 status;
	int call_tasklet = 0;

	status = ioread32(&ch->ch_regs->isr);
	if (!status)
		return IRQ_NONE;

	if (status) {
		/* need to disable intr */
		disable_dma_interrupt(ch);
		/*
		dev_dbg(cdma->pdev, "%s: calling tasklet %#08x\n", __func__,
			status);
		*/
		call_tasklet = 1;
	}

	if (call_tasklet)
		tasklet_schedule(&cdma->tasklet);

	return IRQ_HANDLED;
}

/**
 * xlnx_cdma_setup_dma - Setup the DMA controller
 * @pdev: Controller PCI device structure
 *
 * Initilize the DMA controller, channels, registers with DMA engine,
 * ISR. Initilize DMA controller channels.
 */
static int xlnx_cdma_setup_dma(struct platform_device *pdev)
{
	struct xlnx_cdma_device *dma = dev_get_drvdata(&pdev->dev);
	struct xlnx_cdma_chan *ch = &dma->ch;
	int err;

	/* DMA coherent memory pool for DMA descriptor allocations */
	dma->dma_pool = dma_pool_create("xlnx_mid_dma_desc_pool", 
		&pdev->dev, sizeof(struct xlnx_cdma_desc), 32, 0);
	if (!dma->dma_pool) {
		dev_err(dma->pdev, "%s: dma_pool_create failed\n",__func__);
		err = -ENOMEM;
		goto err_dma_pool;
	}

	INIT_LIST_HEAD(&dma->common.channels);

	/* init CH structure */
	ch->chan.device = &dma->common;
	ch->chan.cookie =  1;
	ch->chan.chan_id = 0;
	dev_dbg(dma->pdev, "%s: init CH %d\n", __func__, 0);

	ch->ch_regs = dma->dma_base;
	ch->dma = dma;
	spin_lock_init(&ch->lock);
	iowrite32(XLNX_CDMA_RST_RST, &ch->ch_regs->rst);
	iowrite32(XLNX_CDMA_DMACR_SINC | XLNX_CDMA_DMACR_DINC,
		&ch->ch_regs->dmacr);

	INIT_LIST_HEAD(&ch->active_list);
	INIT_LIST_HEAD(&ch->queue);
	INIT_LIST_HEAD(&ch->free_list);
	/* mask interrupts */
	disable_dma_interrupt(ch);
	list_add_tail(&ch->chan.device_node, &dma->common.channels);
	
	/* init dma structure */
	dma_cap_zero(dma->common.cap_mask);
	dma_cap_set(DMA_MEMCPY, dma->common.cap_mask);
	/*
	dma_cap_set(DMA_SLAVE, dma->common.cap_mask);
	dma_cap_set(DMA_PRIVATE, dma->common.cap_mask);
	*/
	dma->common.dev = &pdev->dev;
	dma->common.chancnt = 1;

	dma->common.device_alloc_chan_resources = xlnx_cdma_alloc_chan_resources;
	dma->common.device_free_chan_resources = xlnx_cdma_free_chan_resources;

	dma->common.device_tx_status = xlnx_cdma_tx_status;
	dma->common.device_prep_dma_memcpy = xlnx_cdma_prep_memcpy;
	dma->common.device_issue_pending = xlnx_cdma_issue_pending;
	dma->common.device_prep_slave_sg = xlnx_cdma_prep_slave_sg;
	dma->common.device_control = xlnx_cdma_device_control;

	/* register irq */
	err = request_irq(dma->irq, xlnx_cdma_interrupt, IRQF_SHARED,
		"XLNX_CDMA", dma);
	if (err) {
		dev_err(dma->pdev, "%s: request_irq failed: %d\n", __func__, err);
		goto err_irq;
	} else
		dev_dbg(dma->pdev, "%s: registered irq\n", __func__);

	/* register device w/ engine */
	err = dma_async_device_register(&dma->common);
	if (err) {
		dev_err(dma->pdev, "%s: device_register failed: %d\n", __func__,
			err);
		goto err_engine;
	}
	tasklet_init(&dma->tasklet, dma_tasklet, (unsigned long)dma);
	return 0;

err_engine:
	free_irq(dma->irq, dma);
err_irq:
	dma_pool_destroy(dma->dma_pool);
err_dma_pool:
	dev_err(dma->pdev, "%s: setup_dma failed: %d\n", __func__, err);
	return err;

}


static void xlnx_cdma_shutdown(struct platform_device *ofdev)
{
	struct xlnx_cdma_device *device = dev_get_drvdata(&ofdev->dev);

	dma_async_device_unregister(&device->common);
	dma_pool_destroy(device->dma_pool);
	if (device->dma_base)
		iounmap(device->dma_base);
	free_irq(device->irq, device);
}

/**
 * xlnx_cdma_probe - OF Probe
 * @pdev: Controller platform device structure
 * @id: of device id structure
 *
 * Initilize the OF device, query driver data.
 * Call setup_dma to complete contoller and chan initilzation
 */
static int __devinit xlnx_cdma_probe(struct platform_device *ofdev)
{
	struct xlnx_cdma_device *device;
	struct device_node *node = ofdev->dev.of_node;
	int err;

	device = kzalloc(sizeof(*device), GFP_KERNEL);
	if (!device) {
		err = -ENOMEM;
		goto err_kzalloc;
	}

	device->pdev = &ofdev->dev;
	device->dma_base = of_iomap(node, 0);
	if (!device->dma_base) {
		dev_err(device->pdev, "%s: ioremap failed\n", __func__);
		err = -ENOMEM;
		goto err_ioremap;
	}

	device->irq = irq_of_parse_and_map(node, 0);
	if (device->irq == NO_IRQ) {
		dev_dbg(device->pdev, "%s: unable to map irq\n", __func__);
		err = -ENOMEM;
		goto err_ioremap;
	}

	dev_set_drvdata(&ofdev->dev, device);

	err = xlnx_cdma_setup_dma(ofdev);
	if (err)
		goto err_dma;

	return 0;

err_dma:
	iounmap(device->dma_base);
err_ioremap:
	of_dev_put(ofdev);
err_kzalloc:
	kfree(device);
	return err;
}


/**
 * xlnx_cdma_remove - OF remove
 * @pdev: Controller platform device structure
 *
 * Free up all resources and data
 * Call shutdown_dma to complete contoller and chan cleanup
 */
static void __devexit xlnx_cdma_remove(struct platform_device *pdev)
{
	struct xlnx_cdma_device *device = dev_get_drvdata(&pdev->dev);
	xlnx_cdma_shutdown(pdev);
	of_dev_put(pdev);
	kfree(device);
}

/******************************************************************************
* OF stuff
*/
static const struct of_device_id xlnx_cdma_of_ids[] = {
	{ .compatible = "xlnx,xps-central-dma-2.03.a", },
	{}
};
MODULE_DEVICE_TABLE(of, xlnx_cdma_of_ids);

static struct platform_driver xlnx_cdma_of_driver = {
	.driver = {
		.name = "xlnx-central-dma",
		.owner = THIS_MODULE,
		.of_match_table = xlnx_cdma_of_ids,
	},
	.probe = xlnx_cdma_probe,
	.remove = __devexit_p(xlnx_cdma_remove),
};

static int __init xlnx_cdma_init(void)
{
	return platform_driver_register(&xlnx_cdma_of_driver);
}
fs_initcall(xlnx_cdma_init);

static void __exit xlnx_cdma_exit(void)
{
	platform_driver_unregister(&xlnx_cdma_of_driver);
}
module_exit(xlnx_cdma_exit);

MODULE_AUTHOR("Alain Péteut <peteut@space.unibe.ch>");
MODULE_DESCRIPTION("Xilinx XPS CDMA Driver");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.1");

