/*
 * rfm_s.c
 *
 * written for the RFM Tester I/F Module (rfm_s)
 *
 */

#include <asm/byteorder.h>
#include <asm/atomic.h>
#include <asm/delay.h>

#include <linux/of_platform.h>
/* #include <linux/slab.h> */

#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/mutex.h>
#include <linux/spinlock.h>
#include <linux/completion.h>
#include <linux/poll.h>
#include <linux/sysfs.h>
#include <linux/jiffies.h>
#include <linux/circ_buf.h>

#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/stringify.h>

#define DEBUG 1

/* FSL macros */

#define FSL_DEFAULT
#define FSL_NONBLOCKING                          n
#define FSL_EXCEPTION                            e
#define FSL_CONTROL                              c
#define FSL_ATOMIC                               a

#define FSL_NONBLOCKING_EXCEPTION                ne
#define FSL_NONBLOCKING_CONTROL                  nc
#define FSL_NONBLOCKING_ATOMIC                   na
#define FSL_EXCEPTION_CONTROL                    ec
#define FSL_EXCEPTION_ATOMIC                     ea
#define FSL_CONTROL_ATOMIC                       ca

#define FSL_NONBLOCKING_EXCEPTION_CONTROL        nec
#define FSL_NONBLOCKING_EXCEPTION_ATOMIC         nea
#define FSL_NONBLOCKING_CONTROL_ATOMIC           nca
#define FSL_EXCEPTION_CONTROL_ATOMIC             eca

#define FSL_NONBLOCKING_EXCEPTION_CONTROL_ATOMIC neca

#define FSL_ID 0

#define __getfslx(id, flags) \
({ \
	register unsigned long __tmp; \
	asm volatile(__stringify(flags) "get %0, rfsl" __stringify(id) ";" \
	: "=r" (__tmp) \
	: /* no input */ \
	: "rmsr"); \
	__tmp; \
})
#define getfslx(id, flags) __getfslx(id, flags)

#define __putfslx(val, id, flags) \
({ \
	register unsigned long __tmp; \
	asm volatile( \
	__stringify(flags) "put %1, rfsl" __stringify(id) ";" \
	"mfs %0 rmsr;" \
	: "=r" (__tmp) \
	: "r" (val) \
	: "rmsr"); \
	__tmp &= MSR_C; \
	__tmp; \
})
#define putfslx(val, id, flags) __putfslx(val, id, flags)

#define __tgetfslx(id, flags) \
({ \
	register unsigned long __tmp; \
	asm volatile( \
	"t" __stringify(flags) "get %0, rfsl" __stringify(id) ";" \
	: "=r" (__tmp) \
	: /* no inputs */ \
	: "rmsr"); \
	__tmp; \
})
#define tgetfslx(id, flags) __tgetfslx(id, flags)

#define __tputfslx(id, flags) \
({ \
	asm volatile("t" __stringify(flags) "put rfsl" __stringify(id) ";" \
	: /* no outputs */ \
	: /* no inputs */ \
	: "rmsr"); \
})
#define tputfslx(id, flags) __tputfslx(id, flags)

#define __fsl_rx_void(id, flags) \
({ \
	register unsigned long __tmp; \
	asm volatile( \
	"t" __stringify(flags) "get %0, rfsl" __stringify(id) ";" \
	"mfs %0, rmsr;" \
	: "=r" (__tmp) \
	: /* no inputs */ \
	: "rmsr"); \
	__tmp &= (MSR_C | MSR_FSL); \
	__tmp; \
})
#define fsl_rx_void(id, flags) __fsl_rx_void(id, flags)

#define __fsl_tx_full(id, flags) \
({ \
	register unsigned long __tmp; \
	tputfslx(FSL_ID, flags); \
	asm volatile("mfs %0, rmsr;" \
	: "=r" (__tmp) \
	: /* no inputs */ \
	: "rmsr"); \
	__tmp &= MSR_C; \
	__tmp; \
})
#define fsl_tx_full(id, flags) __fsl_tx_full(id, flags)

enum {
	RFM_TX_PROGRESS,
	RFM_RX_EOF,
};

struct rfm_device {
	struct device		*pdev;
	unsigned long		status; /* bitmapped status */
	int			rx_irq;
	int			control_irq;
	union {
		uint32_t config_word;
		struct {
			uint32_t collision_cnt: 8;
			uint32_t pad: 3;
			uint32_t poweroff_red: 1;
			uint32_t poweroff_main: 1;
			uint32_t poweron_red: 1;
			uint32_t poweron_main: 1;
			uint32_t datareq_period: 9;
			uint32_t collision_cnt_rst: 1;
			uint32_t soft_rst: 1;
			uint32_t pps_en: 1;
			uint32_t loopback: 1;
			uint32_t comm_main_red: 1;
			uint32_t datareq_on: 1;
			uint32_t datareq_continuous: 1;
			uint32_t power_main_red: 1;
		} config_fields;
	};
	struct mutex		mutex;
	spinlock_t		lock; /* HW lock */
	wait_queue_head_t	wait;
	struct completion	config_comp;
	struct completion	packet_comp;
	struct circ_buf		read_buf;
};
static struct rfm_device *device = NULL;


#define CIRC_SIZE (16 * PAGE_SIZE)
#define CIRC_MASK (CIRC_SIZE - 1)
#define circ_empty(circ)     ((circ)->head == (circ)->tail)
#define circ_free(circ)      CIRC_SPACE((circ)->head, (circ)->tail, CIRC_SIZE)
#define circ_cnt(circ)       CIRC_CNT((circ)->head, (circ)->tail, CIRC_SIZE)
#define circ_byte(circ, idx) ((circ)->buf[(idx) & CIRC_MASK])
#define circ_cnt_to_end(circ) CIRC_CNT_TO_END((circ)->head & CIRC_MASK, \
	(circ)->tail & CIRC_MASK, CIRC_SIZE)

static inline unsigned long
size_inside_page(unsigned long start, unsigned long size)
{
	unsigned long sz;

	sz = PAGE_SIZE - (start & (PAGE_SIZE - 1));

	return min(sz, size);
}

#if 0
static inline struct rfm_device *to_rfm_device(struct device *pdev)
{
	return container_of(pdev, struct rfm_device, pdev);
}
#endif

#if 0
static inline void
__flush_rx(void)
{
	unsigned long tmp;
	spin_lock(&lock);
	while (!fsl_rx_void(FSL_ID, FSL_NONBLOCKING) ||
			!fsl_rx_void(FSL_ID, FSL_NONBLOCKING_CONTROL))
		tmp = be32_to_cpu(getfslx(FSL_ID, FSL_NONBLOCKING));
	spin_unlock(&lock);
}
#endif

/* Must be called with mutex hold */
static int
rfm_update_config(struct device *dev)
{
	unsigned long flags;

	init_completion(&device->config_comp);
	spin_lock_irqsave(&device->lock, flags);
	while (fsl_tx_full(FSL_ID, FSL_NONBLOCKING)) {
		spin_unlock_irqrestore(&device->lock, flags);
		cond_resched();
		spin_lock_irqsave(&device->lock, flags);
	}
	if (putfslx(cpu_to_be32(device->config_word), FSL_ID, FSL_CONTROL)) {
		spin_unlock_irqrestore(&device->lock, flags);
		return -EIO;
	}
	spin_unlock_irqrestore(&device->lock, flags);

	wait_for_completion(&device->config_comp);

	return 0;
}

/* macro expansion required */
#define __DEVICE_ATTR(_attr, _perm, _show, _store) \
	DEVICE_ATTR(_attr, _perm, _show, _store)

#define DEFINE_MAIN_RED_ATTR_RW(_attr, _perm) \
static ssize_t show_##_attr(struct device *dev, \
		struct device_attribute *attr, char *buf) \
{ \
	unsigned tmp; \
	if (mutex_lock_interruptible(&device->mutex)) \
		return -ERESTARTSYS; \
	tmp = device->config_fields._attr; \
	mutex_unlock(&device->mutex); \
	return sprintf(buf, "%s\n", tmp ? "main" : "red"); \
} \
static ssize_t store_##_attr(struct device *dev, \
		struct device_attribute *attr, const char *buf, size_t count) \
{ \
	if (mutex_lock_interruptible(&device->mutex)) \
		return -ERESTARTSYS; \
	if (!strcasecmp(buf, "main\n") || !strcasecmp(buf, "main")) \
		device->config_fields._attr = 1; \
	else if (!strcasecmp(buf, "red\n") || !strcasecmp(buf, "red")) \
		device->config_fields._attr = 0; \
	else { \
		goto err_out; \
	} \
	if (rfm_update_config(dev)) \
		goto err_out; \
 \
	mutex_unlock(&device->mutex); \
	return strnlen(buf, count); \
err_out: \
	mutex_unlock(&device->mutex); \
	return -EFAULT; \
} \
static __DEVICE_ATTR(_attr, _perm, show_##_attr, store_##_attr)

#define __SHOW_BOOLEAN_ATTR(_attr) \
static ssize_t show_##_attr(struct device *dev, \
		struct device_attribute *attr, char *buf) \
{ \
	unsigned tmp; \
	if (mutex_lock_interruptible(&device->mutex)) \
		return -ERESTARTSYS; \
	tmp = device->config_fields._attr; \
	mutex_unlock(&device->mutex); \
	return sprintf(buf, "%s\n", tmp ? "Y" : "N"); \
}
#define __STORE_BOOLEAN_ATTR(_attr) \
static ssize_t store_##_attr(struct device *dev, \
		struct device_attribute *attr, const char *buf, size_t count) \
{ \
	if (mutex_lock_interruptible(&device->mutex)) \
		return -ERESTARTSYS; \
	if (!strcasecmp(buf, "y\n") || !strcmp(buf, "1\n") || \
			!strcasecmp(buf, "y") || !strcmp(buf, "1")) \
		device->config_fields._attr = 1; \
	 else if (!strcasecmp(buf, "n\n") || !strcmp(buf, "0\n") || \
	 		!strcasecmp(buf, "n") || !strcmp(buf, "0")) \
		device->config_fields._attr = 0; \
	else  \
		goto err_out; \
 \
	if (rfm_update_config(dev)) \
		goto err_out; \
	mutex_unlock(&device->mutex); \
 \
	return strnlen(buf, count); \
err_out: \
	mutex_unlock(&device->mutex); \
	return -EFAULT; \
}
#define __DEFINE_BOOLEAN_ATTR_RW(_attr, _perm) \
__SHOW_BOOLEAN_ATTR(_attr) \
__STORE_BOOLEAN_ATTR(_attr) \
static __DEVICE_ATTR(_attr, _perm, show_##_attr, store_##_attr)
#define DEFINE_BOOLEAN_ATTR_RW(_attr, _perm) \
	__DEFINE_BOOLEAN_ATTR_RW(_attr, _perm)

#define __DEFINE_BOOLEAN_ATTR_RO(_attr, _perm) \
__STORE_BOOLEAN_ATTR(_attr) \
static __DEVICE_ATTR(_attr, _perm, NULL, store_##_attr)
#define DEFINE_BOOLEAN_ATTR_WO(_attr, _perm) \
	__DEFINE_BOOLEAN_ATTR_RO(_attr, _perm)

DEFINE_MAIN_RED_ATTR_RW(comm_main_red, (S_IWUSR | S_IRUGO));
DEFINE_MAIN_RED_ATTR_RW(power_main_red, (S_IWUSR | S_IRUGO));
DEFINE_BOOLEAN_ATTR_RW(pps_en, (S_IWUSR | S_IRUGO));
DEFINE_BOOLEAN_ATTR_RW(loopback, (S_IWUSR | S_IRUGO));
DEFINE_BOOLEAN_ATTR_WO(datareq_on, (S_IWUSR));
DEFINE_BOOLEAN_ATTR_RW(datareq_continuous, (S_IWUSR | S_IRUGO));
DEFINE_BOOLEAN_ATTR_WO(soft_rst, (S_IWUSR));

#define  __STORE_POWER_ONOFF(_attr, _perm, _main_red) \
static ssize_t store_##_attr(struct device *dev, \
		struct device_attribute *attr, const char *buf, size_t count) \
{ \
	unsigned long delay; \
 \
	if (mutex_lock_interruptible(&device->mutex)) \
		return -ERESTARTSYS; \
	device->config_fields.poweron_red = 0; \
	device->config_fields.poweron_main = 0; \
	device->config_fields.poweroff_red = 0; \
	device->config_fields.poweroff_main = 0; \
 \
	if (!strcmp(buf, "on") || !strcmp(buf, "ON")) \
		device->config_fields.poweron_##_main_red = 1; \
	else if (!strcmp(buf, "off") || !strcmp(buf, "OFF")) \
		device->config_fields.poweroff_##_main_red = 1; \
	else \
		goto err_out; \
 \
	/* prevent jitter */ \
	delay = jiffies + 1; \
	while (time_before_eq(jiffies, delay)) \
		cond_resched(); \
 \
	if (rfm_update_config(dev)) \
		goto err_out; \
	delay = jiffies + 64*HZ/1000 + 1; /* 64 ms */ \
	while (time_before_eq(jiffies, delay)) \
		cond_resched(); \
	device->config_fields.poweron_##_main_red = 0; \
	device->config_fields.poweroff_##_main_red = 0; \
	if (rfm_update_config(dev)) \
		goto err_out; \
 \
	mutex_unlock(&device->mutex); \
	return strnlen(buf, count); \
err_out: \
	device->config_fields.poweron_##_main_red = 0; \
	device->config_fields.poweroff_##_main_red = 0; \
	mutex_unlock(&device->mutex); \
	return -EFAULT; \
}
#define __DEFINE_POWER_ONOFF_ATTR_RO(_attr, _perm, _main_red) \
__STORE_POWER_ONOFF(_attr, _perm, _main_red) \
static __DEVICE_ATTR(_attr, _perm, NULL, store_##_attr)
#define DEFINE_POWER_ONOFF_ATTR_RO(_attr, _perm, _main_red) \
	__DEFINE_POWER_ONOFF_ATTR_RO(_attr, _perm, _main_red)

DEFINE_POWER_ONOFF_ATTR_RO(power_red, S_IWUSR, red);
DEFINE_POWER_ONOFF_ATTR_RO(power_main, S_IWUSR, main);

static ssize_t show_datareq_period(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	unsigned long tmp, period;

	if (mutex_lock_interruptible(&device->mutex))
		return -ERESTARTSYS;
	tmp = device->config_fields.datareq_period;
	mutex_unlock(&device->mutex);
	period = 10;
	while (tmp) {
		period--;
		tmp >>= 1;
	}
	/* dev_info(dev, "%s: %ld00 (ms)\n", __func__, period); */

	return sprintf(buf, "%ld00 (ms)\n", period);
}

static ssize_t store_datareq_period(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned long tmp;

	if (sscanf(buf, "%ld", &tmp) != 1)
		goto err_out;
	if ((tmp > 1000) || (tmp < 100))
		goto err_out;
	tmp /= 100;
	/* dev_info(dev, "%s: %ld00 (ms)\n", __func__, tmp); */
	if (mutex_lock_interruptible(&device->mutex))
		return -ERESTARTSYS;
	device->config_fields.datareq_period = (0x100 >> (tmp - 1));
	if (rfm_update_config(dev)) {
		mutex_unlock(&device->mutex);
		return -EFAULT;
	}
	mutex_unlock(&device->mutex);

	return strnlen(buf, count);
err_out:
	return -EINVAL;
}
static DEVICE_ATTR(datareq_period, (S_IWUSR | S_IRUGO),
		show_datareq_period, store_datareq_period);

/* static struct fasync_struct *rfm_async_queue; */

static struct attribute *rfm_attrs[] = {
/*	&dev_attr_collision_cnt.attr, */
	&dev_attr_power_red.attr,
	&dev_attr_power_main.attr,
	&dev_attr_datareq_period.attr,
	&dev_attr_soft_rst.attr,
	&dev_attr_pps_en.attr,
	&dev_attr_loopback.attr,
	&dev_attr_comm_main_red.attr,
	&dev_attr_datareq_on.attr,
	&dev_attr_datareq_continuous.attr,
	&dev_attr_power_main_red.attr,
	NULL,
};
static struct attribute_group rfm_attr_group = {
	.name = "config",
	.attrs = rfm_attrs,
};

static int
rfm_open(struct inode *inode, struct file *filp)
{
	filp->private_data = device;
/*
	if (test_bit(RFM_RD, &device->status))

	if (test_bit(RFM_IS_OPEN, &device->status))
		return -EBUSY;
	set_bit(RFM_IS_OPEN, &device->status);
*/
	return nonseekable_open(inode, filp);
}

static int
rfm_release(struct inode *inode, struct file *filp)
{
/*	clear_bit(RFM_IS_OPEN, &device->status); */
	return 0;
}


static ssize_t
rfm_write(struct file *filp, const char __user *buf, size_t count, loff_t *ppos)
{
	unsigned long tmp, flags;
	unsigned long p = *ppos;
	ssize_t written = 0;
	uint16_t *kbuf, *kp;
	int err = 0;


	if (!buf)
		return -EFAULT;

	/* only up to PAGE_SIZE is supported */
	if (count % 2 || count > PAGE_SIZE)
		return -EINVAL;

	if (mutex_lock_interruptible(&device->mutex))
		return -ERESTARTSYS;

	spin_lock_irqsave(&device->lock, flags);
	while (fsl_tx_full(FSL_ID, FSL_NONBLOCKING)) {
		spin_unlock_irqrestore(&device->lock, flags);
		if (filp->f_flags & O_NONBLOCK) {
			mutex_unlock(&device->mutex);
			return -EBUSY;
		}
		cond_resched();
		spin_lock_irqsave(&device->lock, flags);
	}
	spin_unlock_irqrestore(&device->lock, flags);

	kbuf = (uint16_t *)__get_free_page(GFP_KERNEL);
	if (!kbuf) {
		mutex_unlock(&device->mutex);
		return -ENOMEM;
	}

	if (copy_from_user(kbuf, buf, count)) {
		err = -EFAULT;
		goto unlock_free_page;
	}
	kp = kbuf;

	spin_lock_irqsave(&device->lock, flags);
	while (count > 0) {
		tmp = *kp;

		/* wait until transmitter is ready */
		while (1) {
			if (fsl_tx_full(FSL_ID, FSL_NONBLOCKING))
				continue;
			else
				break;
		}

		if (!written || (count == 2))
			set_bit(16, &tmp);

		if (putfslx(cpu_to_be32(tmp), FSL_ID,
				FSL_NONBLOCKING)) {
			err = -EIO;
			break;
		}

		kp++;
		count -= 2;
		written += 2;
		p += 2;
	}
	spin_unlock_irqrestore(&device->lock, flags);

unlock_free_page:
	mutex_unlock(&device->mutex);
	free_page((unsigned long)kbuf);

	*ppos += written;
	/* dev_info(device->pdev, "%s: wrote %d bytes \n", __func__, written); */

	return written ? : err;
}

static ssize_t
rfm_read(struct file *filp, char __user *buf, size_t count, loff_t *ppos)
{
	unsigned long p = *ppos;
	int ret = 0;

	if (!buf)
		return -EFAULT;

	if (count % 2)
		return -EINVAL;

	if (mutex_lock_interruptible(&device->mutex))
		return -ERESTARTSYS;

	if (circ_empty(&device->read_buf) &&
		completion_done(&device->packet_comp)) {
		init_completion(&device->packet_comp);
		mutex_unlock(&device->mutex);
		/* dev_info(device->pdev, "%s: EOF\n", __func__); */
		return 0; /* EOF */
	}

	/* check if there is any data to read */
	while (circ_empty(&device->read_buf)) {
		if (filp->f_flags & O_NONBLOCK) {
			mutex_unlock(&device->mutex);
			return -EAGAIN;
		}

		/* dev_info(device->pdev, "%s: zzz...\n", __func__); */
		if (wait_event_interruptible(device->wait,
				!circ_empty(&device->read_buf))) {
			mutex_unlock(&device->mutex);
			return -ERESTARTSYS;
		}

		/* if (mutex_lock_interruptible(&device->mutex))
			return -ERESTARTSYS; */
	}

	count = min(count, (size_t)circ_cnt_to_end(&device->read_buf));
	/* dev_info(device->pdev, "%s: junk is %d bytes\n", __func__, count); */
	ret = copy_to_user(buf, &circ_byte(&device->read_buf, device->read_buf.tail), count);
	if (ret) {
		dev_err(device->pdev, "%s: copy_to_user() failed (%d)\n", __func__,ret);
		goto out;
	}
	device->read_buf.tail += count;
	p += count;
	*ppos = p;
out:
	mutex_unlock(&device->mutex);
	/* dev_info(device->pdev, "%s: read %d bytes \n", __func__, read); */

	return count ? : ret;
}

static unsigned int
rfm_poll(struct file *filp, struct poll_table_struct *wait)
{
	unsigned int mask = 0;

	/* dev_info(device->pdev, "%s invoked\n", __func__); */
	if (mutex_lock_interruptible(&device->mutex))
		return -ERESTARTSYS;

	poll_wait(filp, &device->wait, wait);

	if (!circ_empty(&device->read_buf))
		mask |= (POLLIN | POLLRDNORM);

	mutex_unlock(&device->mutex);

	return mask;
}

/*
static int
rfm_fasync(int fd, struct file *filp, int on)
{
	return fasync_helper(fd, filp, on, &rfm_async_queue);
}
*/

static int
rfm_mmap(struct file *filp, struct vm_area_struct *vma)
{
	return -ENOSYS;
}

static irqreturn_t
rx_interrupt(int irq, void *dev_id)
{
/* 	static const unsigned long PACKET_START = 0x10001; */
	static const unsigned long PACKET_END = 0x10002;
	unsigned long tmp = 0;
	unsigned long flags;
	uint16_t *p;

	if (irq != device->rx_irq)
		return IRQ_NONE;

	spin_lock_irqsave(&device->lock, flags);
	while (!fsl_rx_void(FSL_ID, FSL_NONBLOCKING)) {
		tmp = be32_to_cpu(getfslx(FSL_ID,
				FSL_NONBLOCKING));
		spin_unlock_irqrestore(&device->lock, flags);
/*
		circ_byte(&device->read_buf, device->read_buf.head) = (char)((tmp & 0xff00) >> 8);
		circ_byte(&device->read_buf, device->read_buf.head + 1) = (char)(tmp & 0xff);
*/
		/* assumed big endian */
		p = (uint16_t *)&circ_byte(&device->read_buf, device->read_buf.head);
		*p = (uint16_t)(0xffff & tmp);

		device->read_buf.head += sizeof(uint16_t);
		spin_lock_irqsave(&device->lock, flags);
	}
	spin_unlock_irqrestore(&device->lock, flags);

	if (unlikely(tmp == PACKET_END)) {
		complete(&device->packet_comp);
		/* dev_info(device->pdev, "%s: got %ld bytes\n", __func__,
				circ_cnt(&device->read_buf)); */
	}

	wake_up_interruptible(&device->wait);
/*	kill_fasync(&rfm_async_queue, SIGIO, POLL_IN); */

	return IRQ_HANDLED;
}

static irqreturn_t
control_interrupt(int irq, void *dev_id)
{
	unsigned long flags;

	if (irq != device->control_irq)
		return IRQ_NONE;

	spin_lock_irqsave(&device->lock, flags);
	if (!fsl_rx_void(FSL_ID, FSL_NONBLOCKING_CONTROL)) {
		device->config_word = be32_to_cpu(getfslx(FSL_ID,
				FSL_NONBLOCKING_CONTROL));
		spin_unlock_irqrestore(&device->lock, flags);
/*	dev_info(device->pdev, "got config %#x\n",
				device->config_word); */
		complete(&device->config_comp);
	} else {
		spin_unlock_irqrestore(&device->lock, flags);
		BUG();
	}

	return IRQ_HANDLED;
}

/* Driver methods */
static const struct file_operations rfm_ops = {
	.owner = THIS_MODULE,
	.open = rfm_open,
	.release = rfm_release,
	.write = rfm_write,
	.read = rfm_read,
	.poll = rfm_poll,
/*	.fasync = rfm_fasync, */
	.mmap = rfm_mmap,
};

static struct miscdevice miscdev = {
	.minor =  MISC_DYNAMIC_MINOR,
	.name = "rfm",
	.fops = &rfm_ops,
};

static int __devinit
rfm_of_probe(struct platform_device *op)
{
	int ret;

	if (device) {
		dev_err(&op->dev, "only one device supported\n");
		return -EINVAL;
	}
	/* dev->this_device = &op->dev; */

	device = kzalloc(sizeof(*device), GFP_KERNEL);
	if (!device)
		return -ENOMEM;

	device->pdev = &op->dev;
	/* init stuff */
	mutex_init(&device->mutex);
	spin_lock_init(&device->lock);
	init_waitqueue_head(&device->wait);
	init_completion(&device->config_comp);
	init_completion(&device->packet_comp);
	device->read_buf.head = device->read_buf.tail = 0;
	device->read_buf.buf = (char *)__get_free_pages(GFP_KERNEL,
			get_order(CIRC_SIZE));
	if (!device->read_buf.buf) {
		ret = -ENOMEM;
		dev_err(&op->dev, "failed to alloc read_buf\n");
		goto free_device;
	}
	dev_set_drvdata(&op->dev, &miscdev);
	ret = misc_register(&miscdev);
	if (ret) {
		dev_err(&op->dev, "unable to register misc device\n");
		goto free_read_buf;
	}
	ret = sysfs_create_group(&op->dev.kobj, &rfm_attr_group);
	if (ret) {
		dev_err(&op->dev, "failed to create sysfs attributes\n");
		goto free_misc;
	}
	device->rx_irq = irq_of_parse_and_map(op->dev.of_node, 0);
	ret = request_threaded_irq(device->rx_irq, NULL, rx_interrupt,
			IRQF_ONESHOT | IRQF_TRIGGER_HIGH, "rfm",
			device);
	if (ret < 0) {
		dev_err(&op->dev, "Can't register rx irq: %d\n", ret);
		goto free_attr_group;
	}
	device->control_irq = irq_of_parse_and_map(op->dev.of_node, 1);
	ret = request_irq(device->control_irq, control_interrupt,
			IRQF_SHARED | IRQF_TRIGGER_HIGH, "rfm",
			device);
	if (ret < 0) {
		dev_err(&op->dev, "Can't register control irq: %d\n", ret);
		goto free_rx_irq;
	}
	dev_info(&op->dev, "rx irq = %d, control irq = %d\n", device->rx_irq,
			device->control_irq);

	/* soft reset */
	mutex_lock(&device->mutex);
	device->config_fields.soft_rst = 1;
	if (rfm_update_config(&op->dev))
		dev_err(&op->dev, "Can't reset device\n");
	mutex_unlock(&device->mutex);

	return 0;

free_rx_irq:
	free_irq(device->rx_irq, &op->dev);
free_attr_group:
	sysfs_remove_group(&op->dev.kobj, &rfm_attr_group);
free_misc:
	misc_deregister(&miscdev);
free_read_buf:
	free_pages((unsigned long)&device->read_buf.buf, get_order(CIRC_SIZE));
free_device:
	kfree(device);
	return ret;
}

static int __devexit
rfm_of_remove(struct platform_device *op)
{
	sysfs_remove_group(&op->dev.kobj, &rfm_attr_group);
	free_irq(device->rx_irq, device);
	free_irq(device->control_irq, device);
	free_pages((unsigned long)&device->read_buf.buf, get_order(CIRC_SIZE));
	kfree(device);
	device = NULL;
	misc_deregister(&miscdev);

	return 0;
}

static const struct of_device_id rfm_of_ids[] = {
	{ .compatible = "xlnx,rfm-if-1.00.a", },
	{},
};

static struct platform_driver rfm_of_driver = {
	.driver = {
		.name = "rfm",
		.owner = THIS_MODULE,
		.of_match_table = rfm_of_ids,
	},
	.probe = rfm_of_probe,
	.remove = __devexit_p(rfm_of_remove),
};

/* Driver Initialisation */
static int __init rfm_init(void)
{
	return platform_driver_register(&rfm_of_driver);
}

/* Driver Exit */
static void __exit rfm_exit(void)
{
	platform_driver_unregister(&rfm_of_driver);
}

module_init(rfm_init);
module_exit(rfm_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alain Peteut");
MODULE_DESCRIPTION("BELA RFM Module");

